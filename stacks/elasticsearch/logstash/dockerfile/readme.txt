The default Logstash container by Elastic does not include mssql JDBC drivers.
This dockerfile uses the standard Logstash docker container and adds the "mssql-jdbc-7.4.1.jre8.jar" jdbc driver from the /jars directory into the "/usr/share/logstash/logstash-core/lib/jars" directory of the Logstash container.
It also registers and installs the driver according to how Logstash likes it: "RUN logstash-plugin install logstash-input-jdbc" etc.

This image is only needed if a MSSQL database has to be connected to via JDBC. 