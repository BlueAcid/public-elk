-- Logstash NAV Job Queue Log Entry deltasynch query
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SELECT TOP (10000)
      [timestamp]
      ,[Entry No_] AS [entry_no]
      ,[ID] AS [id]
      ,[Object ID to Run] AS [object_iD_to_run]
      ,[Object Type to Run] AS [object_type_to_run]
      ,[Description] AS [description]
      ,[Parameter String] AS [parameter_string]
      ,[User ID] AS [user_id]
      ,[Processed by User ID] AS [processed_by_user_id]
      ,[Start Date_Time] AS [start_date_time]
      ,[End Date_Time] AS [end_date_time]
	  ,CASE
		WHEN [End Date_Time] < [Start Date_Time] THEN 0
		ELSE DATEDIFF(millisecond,[Start Date_Time],[End Date_Time])
	   END AS [duration]
      ,[Status] AS [status]
      ,[Error Message] AS [error_message]
      ,[Posted Successful After Error] AS [posted_successful_after_error]
      ,[Handled] AS [handled]
  FROM [SchuboDB].[dbo].[SCHUBO$Job Queue Log Entry]
  WHERE [timestamp] > :sql_last_value