The "logstash.keystore" file from this directory is missing.

When X-Pack is enabled Elastic nodes allow communication only authenticated, this repos stack only uses basis Auth to do so thus Beats or Logstash need to know about the passwords set in the "bin/elasticsearch-setup-passwords interactive" (1-8_DeployELK_Config-ES-Security) step.

The keystore stores these secrets:
https://www.elastic.co/guide/en/beats/filebeat/current/keystore.html

If created this way (being in a logstash directory it should be similier to bin/logstash-keystore create add <keyname>, then prompted for content of the secret)
The Keyname must be "elasticserach.username" with "elastic" as value, additionally the key "elasticsearch.password" has to exist with the value being the password set for the elastic user in the step where the Elasticsearch passwords where set.