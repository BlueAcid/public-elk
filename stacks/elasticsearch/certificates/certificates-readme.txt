The "elastic-certificates.p12" is missing from this directory:

Check:
https://www.elastic.co/guide/en/elasticsearch/reference/master/certutil.html

To see how to create one. (bin/elasticsearch-certutil ca)