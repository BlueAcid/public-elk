# The spawned VM for docker has to be provided with RAM (disable dynamic RAM) and CPU cores by changing the Hyper-V VM settings
# To do that the node has to be stopped, use docker-machine to stop the node:
docker-machine stop <node-name>

# After changing RAM and CPU settings of the VM start it again with docker-machine:
docker-machine start <node-name>
