﻿$ErrorActionPreference = "Stop"

$initializeSwarmNodeVms = $true
$setSwarmNodeVMSettings = $true
$setSwarmNodeOsSettings = $true
$initializeSwarm = $true
$visualizeSwarm = $true

if ($initializeSwarmNodeVms)
{
    docker-machine create -d hyperv --hyperv-virtual-switch external-switch node01;
    docker-machine create -d hyperv --hyperv-virtual-switch external-switch node02;
    docker-machine create -d hyperv --hyperv-virtual-switch external-switch node03;
}

if ($setSwarmNodeVMSettings)
{
    docker-machine stop node01;
    docker-machine stop node02;
    docker-machine stop node03;
    Set-VMMemory node01 -DynamicMemoryEnabled $false -StartupBytes 3GB;
    Set-VMMemory node02 -DynamicMemoryEnabled $false -StartupBytes 3GB;
    Set-VMMemory node03 -DynamicMemoryEnabled $false -StartupBytes 2GB;
    docker-machine start node01;
    docker-machine start node02;
    docker-machine start node03;
}


if ($setSwarmNodeOsSettings)
{
    docker-machine ssh node01 "sudo sysctl -w vm.max_map_count=262144";
    docker-machine ssh node02 "sudo sysctl -w vm.max_map_count=262144";
    docker-machine ssh node03 "sudo sysctl -w vm.max_map_count=262144";
    docker-machine ssh node01 "echo vm.max_map_count=262144 | sudo tee -a /etc/sysctl.conf";
    docker-machine ssh node02 "echo vm.max_map_count=262144 | sudo tee -a /etc/sysctl.conf";
    docker-machine ssh node03 "echo vm.max_map_count=262144 | sudo tee -a /etc/sysctl.conf";
    docker-machine stop node01;
    docker-machine stop node02;
    docker-machine stop node03;
    docker-machine start node01;
    docker-machine start node02;
    docker-machine start node03;
}

if ($initializeSwarm)
{
    docker-machine ssh node01 "docker swarm init";
    $workerJoinToken = docker-machine ssh node01 "docker swarm join-token -q worker";
    $managerNodeIp = docker-machine ip node01;
    $joinString = 'docker swarm join --token {0} {1}:2377' -f $workerJoinToken, $managerNodeIp;
    docker-machine ssh node02 $joinString
    docker-machine ssh node03 $joinString
}

if ($visualizeSwarm)
{
    docker-machine env node01 | Invoke-Expression;
    docker stack deploy --compose-file 'C:\repos\nav.elk\stacks\swarm-visualizer\docker-compose.yml' swarm-visualizer;
}