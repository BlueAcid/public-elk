﻿$ErrorActionPreference = "Stop"

$addNodeLables = $true
$deployEsStack = $true
$setEsSecurity = $true

if ($addNodeLables)
{
    docker-machine env node01 | invoke-expression;
    docker node update --label-add esmaster=true node01;
    docker node update --label-add esmaster=true node02;
    docker node update --label-add esmaster=true node03;
    docker node update --label-add eskibana=true node01;
    docker node update --label-add esdata=true node02;
    docker node update --label-add esdata=true node03;
    docker node update --label-add logstash.mssql-synch=true node01;
    docker node update --label-add logstash.beat-bridge=true node02;
}

if ($deployEsStack)
{
    docker-machine env node01 | invoke-expression;
    Invoke-Expression "cd C:\repos\nav.elk\stacks\elasticsearch\";
    docker stack deploy --compose-file 'C:\repos\nav.elk\stacks\elasticsearch\docker-compose.yml' elasticsearch;
}

if ($setEsSecurity)
{
    docker-machine env node01 | Invoke-Expression;
    $masterNodeContainerId = docker ps -aqf "name=elasticsearch-master";
    $expression = 'docker exec -it {0} bash;' -f $masterNodeContainerId
    Invoke-Expression $expression;
    bin/elasticsearch-setup-passwords interactive;
}