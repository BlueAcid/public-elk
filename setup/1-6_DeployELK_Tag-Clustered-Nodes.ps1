# In order for the right containers to be placed on the right nodes the designated nodes have to be "tagged" 
# A node capable of hosting data node containers should be tagged with:
docker node update --label-add esdata=true <node-id>

# Kibana hosts:
docker node update --label-add eskibana=true <node-id>

# ES master nodes
docker node update --label-add esmaster=true <node-id>

# Logstash
docker node update --label-add logstash=true <node-id>
