# After the ES cluster initially formed a few security settings have to be done at runtime
# SSH into a ES master node container and run:
# SSH:
docker exec -it <container-id> bash

# Set passwords:
bin/elasticsearch-setup-passwords interactive

# The password for the "kibana" user has to be the same as used in the kibana elasticsearch.password configuration