#Set the max_map_count temporarily in the nodes guest OS
docker-machine ssh <node-name> "sudo sysctl -w vm.max_map_count=262144"

#Set the max_map_count permanenently in the nodes guest OS
vi /etc/sysctl.conf
vm.max_map_count=262144

vi /etc/rc.local 
echo 262144 > /proc/sys/vm/max_map_count

docker-machine ssh node01 "echo vm.max_map_count=262666 | sudo tee -a /etc/sysctl.conf"