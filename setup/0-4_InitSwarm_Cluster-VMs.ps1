# Run the following command on first the designated swarm manager node (in this case docker-machine ssh is used to enter "node01" which will run the command)
docker-machine ssh node01 "docker swarm init"

# Use the resulting generated token to let other nodes join this swarm either as worker or manager
# To join another worker to an existing swarm a join token is needed, to obtain said token run the following needed commands on a swarm manager node
# Get a worker join token:
docker-machine ssh node01 "docker swarm join-token worker"
# Result example:
To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-1ya8l06bjm0gduvu1krftqci2mpoczcvjhcg6omvqb7ivoykxr-7tlfxppuh8fwqkntj5lws9m5j 10.52.65.90:2377


# Get a manager join token:
docker-machine ssh node01 "docker swarm join-token manager"

# To join an existing swarm run the following commands on a node which wants to join the swarm, if a manager token is used the node will join as a manager, if a worker token is used it will join the swarm as a worker
docker-machine ssh node02 "docker swarm join --token SWMTKN-1-1ya8l06bjm0gduvu1krftqci2mpoczcvjhcg6omvqb7ivoykxr-7tlfxppuh8fwqkntj5lws9m5j 10.52.65.90:2377"

# The node should now have joined the swarm, repeat this process until the swarm is clustered