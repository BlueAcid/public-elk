# To visualize the existing Swarm a "visualizer" stack is deployed to the swarm
# To deploy a stack to the swarm the "deploy" command has to be run on a manager node of the swarm
# To get instructions on how to set the powershell environment to operate from the perspective of the manager node run
docker-machine env node01

# Which results state to run the following command
& "C:\Program Files\Docker\Docker\Resources\bin\docker-machine.exe" env node01 | Invoke-Expression
# Long for
docker-machine env <node-name> | Invoke-Expression

# Now we act from inside the manager node
# Navigate in the same powershell window to the repository path ".\repos\nav.elk\stacks\swarm-visualizer" and run
docker stack deploy --compose-file docker-compose.yml swarm-visualizer

# The stack should now be deployed to the swarm running
docker-machine ls

# On any node of the swarm should return the IP´s of all nodes for example:
NAME     ACTIVE   DRIVER   STATE     URL                      SWARM   DOCKER     ERRORS
node1    -        hyperv                                              Unknown
node01   -        hyperv   Running   tcp://10.52.65.90:2376           v19.03.5
node02   -        hyperv   Running   tcp://10.52.65.96:2376           v19.03.5
node03   -        hyperv   Running   tcp://10.52.65.97:2376           v19.03.5

# Accessing the IP of any of the nodes with the port 8080 in a browser will display the swarm visualizer app
# The website shows each node of the swarm as well as their roles, tags, memory and running containers (tasks), 
# the visualizer app should be visible as a running container here