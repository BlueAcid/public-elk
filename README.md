# NAV.ELK
This repository contains everything about ERP´s Elasticsearch cluster

## Architecture overview:
The stack "swarm-visualizer" deploys a through 8080 reachable web app on a single manager node of the swarm its deployed too.

The stack "elasticsearch" deploys a single es master node on each docker swarm node tagged with "esmaster=true" reachable through 9200 and 9300, it also deploys es data nodes on each docker swarm node tagged as "esdata=true".  

Besides these ES nodes a single Kibana container (if one already exists no more are spawned) is also spawned on each docker swarm node tagged with "eskibana=true", having two nodes labled as kibana nodes results in a failover if the first Kibana container fails somehow. 

Kibana and ES master nodes are reachable from outside the swarm network. The data nodes arent. Every request will be directed towards the ES master nodes which then handle the request cluster internally and send the response.
The swarm network is setup in a round robin way, it does not matter which IP is used to access a service inside the swarm stack, the requests port determines which nodes receive the request.

Opening "<OneOfTheDocker-Machine-Node-IPs>:5601" will send a request towards the node machine the IP resolves too. If the node it self does not host a service on port 5601 it will forward the request to its other known nodes until one is found that hosts a service on the given port. Should two service instances on different nodes exist in the cluster requests will be load balanced between them. 

That way all ElasticSearch requests (9200/9300 destination) into the cluster network are handled by the cluster it self, the node which finally processes the result is not known from the outside, this setup guarantees all requests are forwarded not to a data but always to a master node first. 

## Folder structure:
| Level 0 | Level 1     | Level 2 |
| ------- | ----------- | ------- |
| root    |             |         |
|         | setup       |         |
|         | stacks      |         |
|         | maintenance |         |

setup: Contains everything needed to setup machines, form a swarm cluster, deploy and initialiy start the ES stack  
stacks: Contains the stacks/services (docker-compose.yml) which are part of the cluster  
maintenance: Contains everything needed to configure and keep the cluster running  

## Security lessening
If a secured cluster is not needed set "xpack.security.enabled=true" to "xpack.security.enabled=false" as well as "xpack.security.transport.ssl.enabled=true" to "xpack.security.transport.ssl.enabled=false" in all references of the Elasticsearch stacks docker-compose file. That way no certificates or keystores are needed to allow communication with the cluster.

## Ressources (hardware)
The docker-compoes file of the elasticsearch stack controlls the amount of RAM and heap provided to elasticsearch. 
"ES_JAVA_OPTS" environment settings in the docker-compose have to be altered to own needs.

Similier to the "vm.max_map_count" OS setting of the node VMs (set through one of the powershell setup scripts (0-3_InitSwarm_VM-OS-Settings))